# Résolution de labyrinthes

<div align="center">
<img width="500" height="400" src="Recherche_Operationnelle.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en DUT INFO 2 à l'IUT de Nantes dans le cadre du module "Recherche opérationnelle et aide à la décision" durant l'année 2017-2018. \
L’objectif de ce projet est de nous familiariser avec la formalisation de problèmes, la résolution
non informée par les algorithmes d’exploration en largeur et en profondeur ainsi que la
résolution informée avec la définition de fonctions heuristiques et la recherche de solutions
par l’algorithme A∗ . Pour ce faire, j'ai appliqué ces notions à la recherche d’une
solution optimale dans un labyrinthe.
Un labyrinthe comporte des cases vides et des cases pleines correspondant à des murs,
chaque case pouvant être identifiée par ses coordonnées.
Le but du jeu consiste à trouver le chemin le plus court allant de l’entrée du labyrinthe
à la sortie de celui-ci.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par une enseignante de l'IUT de Nantes :
- Solen QUINIOU : solen.quiniou@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Recherche opérationnelle et aide à la décision" du DUT INFO 2.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 01/06/2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Exploration A* (frontière implémentée avec une file prioritaire) ;
- Exploration en largeur (frontière implémentée avec une file) ;
- Exploration en profondeur (frontière implémentée avec une pile).

## Objectifs

Le programme Java doit résoudre des problèmes de jeux en cherchant une solution par exploration en arbre d’une situation donnée grâce à une stratégie mise en place. \
Cette exploration se fait grâce à des algorithmes de recherche :
- Non informée (ou aveugle) : nous ne disposons d’aucune information sur le problème hormis sa solution.
- Informée (ou heuristique) : nous avons une idée de la direction à prendre pour guider la recherche des solutions.

Dans la recherche non informée, nous avons deux parcours possibles :
- Exploration en largeur : nous commençons par développer le nœud racine puis ses successeurs, que je nommerais par ses feuilles, puis tous les successeurs de ses successeurs.
- Exploration en profondeur : nous commençons toujours par développer le nœud le plus profond de la frontière courante de l’arbre de recherche situé au plus profond de l’arbre, c’est à dire les nœuds qui n’ont pas de successeurs. Lorsque ces nœuds ont été développés, ils sont supprimés de la frontière et l’exploration reprend en amont, au prochain nœud le moins profond qui a encore des successeurs inexploités.

Quant à la recherche informée, nous avons un parcours possible :
- Exploration A* : Nous évaluons les nœuds en combinant g(n), le coût pour atteindre le nœud n, et h(n), le coût pour aller du nœud n au but

Le programme implémente ces algorithmes de recherche que ce soit informée ou non pour essayer de trouver la solution au problème.

