package application;
import java.util.ArrayList;

import situation.*;
import probleme.*;
import solution.*;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Application permet de définir les situations, les problèmes et les stratégies
* pour effectuer une recherche et ainsi résoudre les problemes des situations s'il y en a
*/
public class Application {

	public static void main(String[] args) 
	{		
		try
		{
			//Situation du Labyrinthe1 5x6 (1,2,3,4)
			
			//Etat Initial
			Labyrinthe labyrintheInitiale1 = Factory.creerLabyrintheInitiale1();
			
			//Etats Finaux
			Labyrinthe labyrintheFinale1 = Factory.creerLabyrintheFinale1();
			ArrayList<Situation> labyrintheFinaux1 = new ArrayList<Situation>();
			labyrintheFinaux1.add(labyrintheFinale1);
			
			//Probleme du labyrinthe
			Probleme problemeLabyrinthe1 = new Probleme(labyrintheInitiale1,labyrintheFinaux1);
			
			//Les actions du labyrinthe
			ArrayList<Integer> actionsLabyrinthe1 = new ArrayList<Integer>();
			actionsLabyrinthe1.add(1);
			actionsLabyrinthe1.add(2);
			actionsLabyrinthe1.add(3);
			actionsLabyrinthe1.add(4);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées
			Strategie strategieLabyrinthe1 = new Strategie(actionsLabyrinthe1,labyrintheInitiale1);
			
			Recherche.explorer_en_arbre(problemeLabyrinthe1,strategieLabyrinthe1);
			
			//Situation du Labyrinthe2 5x6 (4,3,2,1)
			
			//Etat Initial
			Labyrinthe labyrintheInitiale2 = Factory.creerLabyrintheInitiale1();
			
			//Etats Finaux
			Labyrinthe labyrintheFinale2 = Factory.creerLabyrintheFinale1();
			ArrayList<Situation> labyrintheFinaux2 = new ArrayList<Situation>();
			labyrintheFinaux2.add(labyrintheFinale2);
			
			//Probleme du labyrinthe
			Probleme problemeLabyrinthe2 = new Probleme(labyrintheInitiale2,labyrintheFinaux2);
			
			//Les actions du labyrinthe
			ArrayList<Integer> actionsLabyrinthe2 = new ArrayList<Integer>();
			actionsLabyrinthe2.add(4);
			actionsLabyrinthe2.add(3);
			actionsLabyrinthe2.add(2);
			actionsLabyrinthe2.add(1);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées
			Strategie strategieLabyrinthe2 = new Strategie(actionsLabyrinthe2,labyrintheInitiale2);
			
			Recherche.explorer_en_arbre(problemeLabyrinthe2,strategieLabyrinthe2);
			
			//Situation du Labyrinthe3 20x20 (1,2,3,4)
			
			//Etat Initial
			Labyrinthe labyrintheInitiale3 = Factory.creerLabyrintheInitiale2();
			
			//Etats Finaux
			Labyrinthe labyrintheFinale3 = Factory.creerLabyrintheFinale2();
			ArrayList<Situation> labyrintheFinaux3 = new ArrayList<Situation>();
			labyrintheFinaux3.add(labyrintheFinale3);
			
			//Probleme du labyrinthe
			Probleme problemeLabyrinthe3 = new Probleme(labyrintheInitiale3,labyrintheFinaux3);
			
			//Les actions du labyrinthe
			ArrayList<Integer> actionsLabyrinthe3 = new ArrayList<Integer>();
			actionsLabyrinthe3.add(1);
			actionsLabyrinthe3.add(2);
			actionsLabyrinthe3.add(3);
			actionsLabyrinthe3.add(4);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées
			Strategie strategieLabyrinthe3 = new Strategie(actionsLabyrinthe3,labyrintheInitiale3);
			
			Recherche.explorer_en_arbre(problemeLabyrinthe3,strategieLabyrinthe3);
			
			//Situation du Labyrinthe4 20x20 (1,2,3,4)
			
			//Etat Initial
			Labyrinthe labyrintheInitiale4 = Factory.creerLabyrintheInitiale2();
			
			//Etats Finaux
			Labyrinthe labyrintheFinale4 = Factory.creerLabyrintheFinale2();
			ArrayList<Situation> labyrintheFinaux4 = new ArrayList<Situation>();
			labyrintheFinaux4.add(labyrintheFinale4);
			
			//Probleme du labyrinthe
			Probleme problemeLabyrinthe4 = new Probleme(labyrintheInitiale4,labyrintheFinaux4);
			
			//Les actions du labyrinthe
			ArrayList<Integer> actionsLabyrinthe4 = new ArrayList<Integer>();
			actionsLabyrinthe4.add(4);
			actionsLabyrinthe4.add(3);
			actionsLabyrinthe4.add(2);
			actionsLabyrinthe4.add(1);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées
			Strategie strategieLabyrinthe4 = new Strategie(actionsLabyrinthe4,labyrintheInitiale4);
			
			Recherche.explorer_en_arbre(problemeLabyrinthe4,strategieLabyrinthe4);
				
			//Situation du Tablut (1, 2, 3, 4)
			
			//Etat Initial
			Tablut tablutInitiale1 = Factory.creerTablutInitiale();
			
			//Etats Finaux
			Tablut tablutFinale1 = Factory.creerTablutFinale();
			ArrayList<Situation> tablutFinaux1 = new ArrayList<Situation>();
			tablutFinaux1.add(tablutFinale1);
			
			//Probleme du tablut
			Probleme problemeTablut1 = new Probleme(tablutInitiale1,tablutFinaux1);
			
			//Les actions du tablut
			ArrayList<Integer> actionsTablut1 = new ArrayList<Integer>();
			actionsTablut1.add(1);
			actionsTablut1.add(2);
			actionsTablut1.add(3);
			actionsTablut1.add(4);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées dans l'ordre
			Strategie strategieTablut1 = new Strategie(actionsTablut1,tablutInitiale1);
			
			Recherche.explorer_en_arbre(problemeTablut1,strategieTablut1);
			
			//Situation du Tablut (4, 3, 2, 1)
			
			//Etat Initial
			Tablut tablutInitiale2 = Factory.creerTablutInitiale();
			
			//Etats Finaux
			Tablut tablutFinale2 = Factory.creerTablutFinale();
			ArrayList<Situation> tablutFinaux2 = new ArrayList<Situation>();
			tablutFinaux2.add(tablutFinale2);
			
			//Probleme du tablut
			Probleme problemeTablut2 = new Probleme(tablutInitiale2,tablutFinaux2);
			
			//Les actions du tablut
			ArrayList<Integer> actionsTablut2 = new ArrayList<Integer>();
			actionsTablut2.add(4);
			actionsTablut2.add(3);
			actionsTablut2.add(2);
			actionsTablut2.add(1);
			
			//La stratégie du labyrinthe selon la liste d'actions a effectuées dans l'ordre
			Strategie strategieTablut2 = new Strategie(actionsTablut2,tablutInitiale2);
			
			Recherche.explorer_en_arbre(problemeTablut2,strategieTablut2);
		}
		catch(SituationException e)
		{
			System.out.println(e.getMessage());
		}
		catch(ProblemeException e)
		{
			System.out.println(e.getMessage());
		}
		catch(StrategieException e)
		{
			System.out.println(e.getMessage());
		}
		catch(Echec e)
		{
			System.out.println(e.getMessage());
		}

	}
	
}
