package application;
import java.util.ArrayList;

import situation.*;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Factory permet de construire l'état initial et le(s) état(s) finale(s) des situations
*/
public class Factory 
{
	public static Labyrinthe creerLabyrintheInitiale1() throws SituationException
	{
		try
		{
		ArrayList<Case> casesLabyrintheInitiale = new ArrayList<Case>();
		for(int y=0 ; y<6 ; y++)
		{
			for(int x=0 ; x<5 ; x++)
			{
				if((x==0 && y==2) || (x==0 && y==3) || (x==2 && y==1) || 
				   (x==2 && y==2) || (x==2 && y==3) || (x==2 && y==4) || 
				   (x==3 && y==4) || (x==4 && y==1) || (x==4 && y==2))
				{
					casesLabyrintheInitiale.add(new Case(x,y,false," M "));
				}
				else if((x==0 && y==1))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"E/D"));
				}
				else if((x==4 && y==3))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true," S "));
				}
				else
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"   "));
				}
			}
		}
		
		Case entree = new Case(0,1,true," E ");
		Case deplacementInitiale = new Case(0,1,true," D ");
		Case sortie = new Case(4,3,true," S ");

		return new Labyrinthe(casesLabyrintheInitiale,entree,sortie,deplacementInitiale,5,6);
		
		}
		catch(SituationException e)
		{
			throw new SituationException(e.getMessage());
		}
		catch(CaseException e)
		{
			throw new SituationException(e.getMessage());
		}
	}
	
	public static Labyrinthe creerLabyrintheInitiale2() throws SituationException
	{
		try
		{
		ArrayList<Case> casesLabyrintheInitiale = new ArrayList<Case>();
		for(int y=0 ; y<20 ; y++)
		{
			for(int x=0 ; x<20 ; x++)
			{
				if((x==0 && y==0) || (x==0 && y==4) || (x==0 && y==8) || (x==0 && y==12) || (x==0 && y==16) ||
				   (x==4 && y==0) || (x==4 && y==4) || (x==4 && y==8) || (x==4 && y==12) || (x==4 && y==16) ||
				   (x==8 && y==0) || (x==8 && y==4) || (x==8 && y==8) || (x==8 && y==12) || (x==8 && y==16) || 
				   (x==12 && y==0) || (x==12 && y==4) || (x==12 && y==8) || (x==12 && y==12) || (x==12 && y==16) ||
				   (x==16 && y==0) || (x==16 && y==4) || (x==16 && y==8) || (x==16 && y==12) || (x==16 && y==16))
				{
					casesLabyrintheInitiale.add(new Case(x,y,false," M "));
				}
				else if((x==0 && y==1))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"E/D"));
				}
				else if((x==18 && y==16))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true," S "));
				}
				else
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"   "));
				}
			}
		}
		
		Case entree = new Case(0,1,true," E ");
		Case deplacementInitiale = new Case(0,1,true," D ");
		Case sortie = new Case(18,16,true," S ");

		return new Labyrinthe(casesLabyrintheInitiale,entree,sortie,deplacementInitiale,20,20);
		
		}
		catch(SituationException e)
		{
			throw new SituationException(e.getMessage());
		}
		catch(CaseException e)
		{
			throw new SituationException(e.getMessage());
		}
	}
	
	public static Labyrinthe creerLabyrintheFinale1() throws SituationException
	{
		try
		{
		ArrayList<Case> casesLabyrintheFinale = new ArrayList<Case>();
		for(int y=0 ; y<6 ; y++)
		{
			for(int x=0 ; x<5 ; x++)
			{
				if((x==0 && y==2) || (x==0 && y==3) || (x==2 && y==1) || 
				   (x==2 && y==2) || (x==2 && y==3) || (x==2 && y==4) || 
				   (x==3 && y==4) || (x==4 && y==1) || (x==4 && y==2))
				{
					casesLabyrintheFinale.add(new Case(x,y,false," M "));
				}
				else if((x==0 && y==1))
				{
					casesLabyrintheFinale.add(new Case(x,y,true," E "));
				}
				else if((x==4 && y==3))
				{
					casesLabyrintheFinale.add(new Case(x,y,true,"S/D"));
				}
				else
				{
					casesLabyrintheFinale.add(new Case(x,y,true,"   "));
				}
			}
		}
		
		Case entree = new Case(0,1,true," E ");
		Case deplacementFinale = new Case(4,3,true," D ");
		Case sortie = new Case(4,3,true," S ");

		return new Labyrinthe(casesLabyrintheFinale,entree,sortie,deplacementFinale,5,6);
		
		}
		catch(CaseException e)
		{
			throw new SituationException(e.getMessage());
		}
		catch(SituationException e)
		{
			throw new SituationException(e.getMessage());
		}
	}
	
	public static Labyrinthe creerLabyrintheFinale2() throws SituationException
	{
		try
		{
		ArrayList<Case> casesLabyrintheInitiale = new ArrayList<Case>();
		for(int y=0 ; y<20 ; y++)
		{
			for(int x=0 ; x<20 ; x++)
			{
				if((x==0 && y==0) || (x==0 && y==4) || (x==0 && y==8) || (x==0 && y==12) || (x==0 && y==16) ||
				   (x==4 && y==0) || (x==4 && y==4) || (x==4 && y==8) || (x==4 && y==12) || (x==4 && y==16) ||
				   (x==8 && y==0) || (x==8 && y==4) || (x==8 && y==8) || (x==8 && y==12) || (x==8 && y==16) || 
				   (x==12 && y==0) || (x==12 && y==4) || (x==12 && y==8) || (x==12 && y==12) || (x==12 && y==16) ||
				   (x==16 && y==0) || (x==16 && y==4) || (x==16 && y==8) || (x==16 && y==12) || (x==16 && y==16))
				{
					casesLabyrintheInitiale.add(new Case(x,y,false," M "));
				}
				else if((x==0 && y==1))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"E/D"));
				}
				else if((x==18 && y==16))
				{
					casesLabyrintheInitiale.add(new Case(x,y,true," S "));
				}
				else
				{
					casesLabyrintheInitiale.add(new Case(x,y,true,"   "));
				}
			}
		}
		
		Case entree = new Case(0,1,true," E ");
		Case deplacementFinale = new Case(18,16,true," D ");
		Case sortie = new Case(18,16,true," S ");

		return new Labyrinthe(casesLabyrintheInitiale,entree,sortie,deplacementFinale,20,20);
		
		}
		catch(SituationException e)
		{
			throw new SituationException(e.getMessage());
		}
		catch(CaseException e)
		{
			throw new SituationException(e.getMessage());
		}
	}
	
	public static Tablut creerTablutInitiale() throws SituationException
	{
		ArrayList<Case> casesTablutInitiale = new ArrayList<Case>();
		try
		{
			casesTablutInitiale.add(new Case(0,0,false,"5"));
			casesTablutInitiale.add(new Case(1,0,false,"4"));
			casesTablutInitiale.add(new Case(2,0,true," "));
			casesTablutInitiale.add(new Case(0,1,false,"3"));
			casesTablutInitiale.add(new Case(1,1,false,"2"));
			casesTablutInitiale.add(new Case(2,1,false,"1"));
		}
		catch(CaseException e)
		{
			
		}
		return new Tablut(casesTablutInitiale,3,2);
	}
	
	public static Tablut creerTablutFinale() throws SituationException
	{
		ArrayList<Case> casesTablutFinale = new ArrayList<Case>();
		try
		{
			casesTablutFinale.add(new Case(0,0,true," "));
			casesTablutFinale.add(new Case(1,0,false,"1"));
			casesTablutFinale.add(new Case(2,0,false,"2"));
			casesTablutFinale.add(new Case(0,1,false,"3"));
			casesTablutFinale.add(new Case(1,1,false,"4"));
			casesTablutFinale.add(new Case(2,1,false,"5"));
		}
		catch(CaseException e)
		{
			
		}

		return new Tablut(casesTablutFinale,3,2);
	}
}
