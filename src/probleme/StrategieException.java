package probleme;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe StrategieException permet de lancer une exception relative à la classe Strategie
* @see Strategie
*/
@SuppressWarnings("serial")
public class StrategieException extends Exception
{
	public StrategieException(String message)
	{
		super(message);
	}
}
