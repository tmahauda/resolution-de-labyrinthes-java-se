package probleme;
import java.util.ArrayList;
import situation.Situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Probleme représente un problème à résoudre à partir d'une situation initial
* pour obtenir un des états finaux
*/
public class Probleme
{
	/**
     * Un état initial (un état décrit une Situation à l'aide de plusieurs attributs)
     * @see Situation
     */
	private Situation etatInitiale;
	
	/**
     * Un ensemble d'états finaux (ou buts) ou bien un test d'état final qui détermine si un état donné est ou non un état final
     * @see Situation
     */
	private ArrayList<Situation> etatsFinaux;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon trois conditions :
     * - Si un des états finaux contient l'état initial, alors le problème ne se pose plus
     * - Si l'état initial ne comporte aucune action à appliquer pour rejoindre un des états finales, alors on ne peut pas résoudre le problème
     * - Si l'ensemble d'états finaux ne comportent aucun état, alors on ne peut pas connaitre la solution au probleme
     * @param etatInitiale l'état initiale
     * @param etatsFinaux les états finaux
     * @see Situation
     * @see ProblemeException 
     * @throws ProblemeException Si les conditions ne sont pas respectés
     */
	public Probleme(Situation etatInitiale, ArrayList<Situation> etatsFinaux) throws ProblemeException
	{
		boolean creerProbleme = true;
		for(Situation etatFinale : etatsFinaux)
		{
			if(etatInitiale.equals(etatFinale))
			{
				creerProbleme = false;
			}
		}
		
		if(creerProbleme)
		{
			if(etatInitiale.verifierOperateurs())
			{
				if(!etatsFinaux.isEmpty())
				{
					this.etatInitiale = etatInitiale;
					this.etatsFinaux = etatsFinaux;
				}
				else throw new ProblemeException("L'ensemble d'états finaux ne comporte aucun état");
			}
			else throw new ProblemeException("Le problème ne comporte aucune solution car l'état initial ne peut pas être modifié");
		}
		else throw new ProblemeException("Vous avez déjà la solution !");
	}
	
	/**
     * Sélecteur qui permet de récupérer l'état initial
     * @return l'état initiale
     */
	public Situation getEtatInitiale() 
	{
		return etatInitiale;
	}
	
	/**
     * Modificateur qui permet de modifier l'état initial
     * @param etatInitiale le nouvel état initiale
     */
	public void setEtatInitiale(Situation etatInitiale) 
	{
		this.etatInitiale = etatInitiale;
	}
	
	/**
     * Sélecteur qui permet de récupérer la liste d'états finaux
     * @return la liste d'états finaux
     */
	public ArrayList<Situation> getEtatsFinaux() {
		return this.etatsFinaux;
	}
	
	/**
     * Modificateur qui permet de modifier la liste d'etats finaux
     * @param etatsFinaux la nouvelle liste d'états finaux
     */
	public void setEtatsFinal(ArrayList<Situation> etatsFinaux) 
	{
		this.etatsFinaux = etatsFinaux;
	}
	
	/**
     * Méthode qui permet de récupérer un état final de la liste
     * @param index pour récupérer l'état à un index
     * @return un état final
     */
	public Situation getEtatFinale(int index) 
	{
		return this.etatsFinaux.get(index);
	}
	
	/**
     * Méthode qui permet de récupérer le nombre d'états finaux
     * @return la taille
     */
	public int getNombreEtatsFinaux()
	{
		return this.etatsFinaux.size();
	}
	
}
