package probleme;
import java.util.ArrayList;

import situation.Situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Strategie représente une stratégie de résolution du problème
*/
public class Strategie 
{	
	/**
     * Une liste d'actions ordonnée pour appliquer les opérateurs
     */
	private ArrayList<Integer> actions;

	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon deux conditions :
     * - Si la liste d'action est vide alors on ne peut pas résoudre le problème
     * - Si une des actions n'est pas reconnu par l'état initial alors il faut revoir la liste d'actions
     * @param actions les actions pour appliquer l'opérateur
     * @param etatInitial l'état initial
     * @see Situation
     * @see StrategieException
     * @throws StrategieException Si les conditions ne sont pas respectés
     */
	public Strategie(ArrayList<Integer> actions, Situation etatInitial) throws StrategieException
	{
		Situation test = etatInitial.cloner();
		if(actions != null && !actions.isEmpty())
		{
			for(int a : actions)
			{
				if(test.appliquerOperateurs(a) == "Opérateur inconnu")
				{
					throw new StrategieException("L'action "+ a +" n'a pas été reconnu !");
				}
			}
			this.actions = actions;
		}
		else throw new StrategieException("Il faut une liste d'actions non vide pour créer une stratégie !");
	}
	
	/**
     * Méthode qui permet de récupérer une action par son index
     * @param index pour récupérer l'action à un index
     * @return action
     */
	public int getAction(int index)
	{
		return this.actions.get(index);
	}
	
	/**
     * Méthode qui permet de récupérer le nombre d'actions
     * @return le nombre d'actions
     */
	public int getNombreActions()
	{
		return this.actions.size();
	}
	
	@Override
	public String toString()
	{
		String toString = "";
		for(int i : this.actions)
		{
			toString = toString + i + " ";
		}
		
		return toString.trim();
	}
}
