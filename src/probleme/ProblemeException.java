package probleme;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe ProblemeException permet de lancer une exception relative à la classe Probleme
* @see Probleme
*/
@SuppressWarnings("serial")
public class ProblemeException extends Exception
{
	public ProblemeException(String message)
	{
		super(message);
	}
}
