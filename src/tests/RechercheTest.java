package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import application.Factory;
import probleme.Probleme;
import probleme.Strategie;
import situation.Labyrinthe;
import situation.Situation;
import solution.Frontiere;
import solution.FrontiereException;
import solution.Noeud;
import solution.Recherche;

public class RechercheTest {
	
	private Labyrinthe etatFinale;
	private Labyrinthe etatInitiale;
	private Probleme probleme;
	private Strategie strategie;

	@Before
	public void setUp() throws Exception 
	{
		this.etatInitiale = Factory.creerLabyrintheInitiale1();
		this.etatFinale = Factory.creerLabyrintheFinale1();
		ArrayList<Situation> etatsFinaux = new ArrayList<Situation>();
		etatsFinaux.add(this.etatFinale);
		this.probleme = new Probleme(this.etatInitiale,etatsFinaux);
		ArrayList<Integer> actions = new ArrayList<Integer>();
		actions.add(1);
		actions.add(2);
		actions.add(3);
		actions.add(4);
		this.strategie = new Strategie(actions, this.etatInitiale);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExplorer_en_arbre() {
		fail("Not yet implemented");
	}

	@Test
	public void testExplorer_en_graphe_profondeur() {
		fail("Not yet implemented");
	}

	@Test
	public void testExplorer_en_graphe_largeur() {
		fail("Not yet implemented");
	}

	@Test
	public void testExplorer_en_graphe_A_Heuristique_Manhattan() {
		fail("Not yet implemented");
	}

	@Test
	public void testExplorer_en_graphe_A_Heuristique_Euclidienne() {
		fail("Not yet implemented");
	}

	@Test
	public void testExplorer_en_graphe() {
		fail("Not yet implemented");
	}

	@Test
	public void testVerifierSolution1() {
		assertTrue(Recherche.verifierSolution(this.etatFinale, this.probleme));
	}
	
	@Test
	public void testVerifierSolution2() {
		assertFalse(Recherche.verifierSolution(this.etatInitiale, this.probleme));
	}
	
	@Test
	public void testVerifierDeveloppementFeuilles1() {
		ArrayList<Noeud> listeFerme = new ArrayList<Noeud>();
		Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
		assertTrue(Recherche.verifierDeveloppementFeuilles(noeud, listeFerme));
	}
	
	@Test
	public void testVerifierDeveloppementFeuilles2() {
		ArrayList<Noeud> listeFerme = new ArrayList<Noeud>();
		Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
		listeFerme.add(noeud);
		assertFalse(Recherche.verifierDeveloppementFeuilles(noeud, listeFerme));
	}

	// Avec 4 feuilles
	@Test
	public void testDevelopperFeuilles1() {
		try
		{
			Frontiere pile = new Frontiere(1);
			this.etatInitiale.getCase(2, 4).setEstVide(true);
			this.etatInitiale.getCaseDeplacement().setX(1);
			this.etatInitiale.getCaseDeplacement().setY(4);
			Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
			Recherche.developperFeuilles(noeud, this.strategie, pile);
			assertEquals(4,pile.getTaille());
		} 
		catch(FrontiereException e) {}
	}
	
	// Avec 3 feuilles
	@Test
	public void testDevelopperFeuilles2() {
		try
		{
			Frontiere pile = new Frontiere(1);
			this.etatInitiale.getCaseDeplacement().setX(1);
			this.etatInitiale.getCaseDeplacement().setY(4);
			Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
			Recherche.developperFeuilles(noeud, this.strategie, pile);
			assertEquals(3,pile.getTaille());
		} 
		catch(FrontiereException e) {}
	}
	
	// Avec 2 feuilles
	@Test
	public void testDevelopperFeuilles3() {
		try
		{
			Frontiere pile = new Frontiere(1);
			this.etatInitiale.getCaseDeplacement().setX(2);
			this.etatInitiale.getCaseDeplacement().setY(0);
			Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
			Recherche.developperFeuilles(noeud, this.strategie, pile);
			assertEquals(2,pile.getTaille());
		} 
		catch(FrontiereException e) {}
	}
	
	// Avec 1 feuille
	@Test
	public void testDevelopperFeuilles4() {
		try
		{
			Frontiere pile = new Frontiere(1);
			this.etatInitiale.getCaseDeplacement().setX(4);
			this.etatInitiale.getCaseDeplacement().setY(0);
			Noeud noeud = new Noeud(this.etatInitiale, null, "Aucun", 0, 0);
			Recherche.developperFeuilles(noeud, this.strategie, pile);
			assertEquals(1,pile.getTaille());
		} 
		catch(FrontiereException e) {}
	}
	
	// Avec 0 feuille = impossible car on vérifie avant si il y a au moins un opérateur applicable

}
