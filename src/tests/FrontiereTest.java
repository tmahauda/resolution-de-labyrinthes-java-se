package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import situation.*;
import solution.Frontiere;
import solution.FrontiereException;
import solution.Noeud;
import application.Factory;

public class FrontiereTest {

	private Frontiere pile;
	private Frontiere file;
	private Frontiere file_priorite;
	private Labyrinthe etat;
	private Noeud parent;
	
	@Before
	public void setUp() throws FrontiereException
	{
		try
		{
			this.pile = new Frontiere(1);
			this.file = new Frontiere(2);
			this.file_priorite = new Frontiere(3);
			this.etat = Factory.creerLabyrintheInitiale1();
			this.parent = new Noeud(this.etat,null,"aucun",0,0);
			Noeud.setChoixHeuristique(1);
		}
		catch(FrontiereException e)
		{
			System.out.println(e.getMessage());
		}
		catch(SituationException e)
		{
			System.out.println(e.getMessage());
		}
	}

	@After
	public void tearDown() 
	{
	}

	@Test(expected=FrontiereException.class)
	public void testFrontiere() throws FrontiereException
	{
		new Frontiere(4);
	}

	@Test
	public void testAjouterPile() 
	{
		Noeud enQueue = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.pile.ajouter(enQueue);
		Noeud enTete = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.pile.ajouter(enTete);
		assertEquals(2,this.pile.getTaille());
		assertSame(enTete,this.pile.getNoeud());
	}
	
	@Test
	public void testSupprimerVide() 
	{
		Noeud videPile = this.pile.supprimer();
		Noeud videFile = this.file.supprimer();
		Noeud videFilePrioritaire = this.file_priorite.supprimer();
		assertEquals(0,this.pile.getTaille());
		assertEquals(0,this.file.getTaille());
		assertEquals(0,this.file_priorite.getTaille());
		assertNull(videPile);
		assertNull(videFile);
		assertNull(videFilePrioritaire);
	}
	
	@Test
	public void testSupprimerPile() 
	{
		Noeud enQueue = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.pile.ajouter(enQueue);
		Noeud enTete = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.pile.ajouter(enTete);
		Noeud supprime = this.pile.supprimer();
		assertEquals(1,this.pile.getTaille());
		assertSame(enTete,supprime);
	}
	
	@Test
	public void testAjouterFile() 
	{
		Noeud enTete = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file.ajouter(enTete);
		Noeud enQueue = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file.ajouter(enQueue);
		assertEquals(2,this.file.getTaille());
		assertSame(enTete,this.file.getNoeud());
	}
	
	@Test
	public void testSupprimerFile() 
	{
		Noeud enTete = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file.ajouter(enTete);
		Noeud enQueue = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file.ajouter(enQueue);
		Noeud supprime = this.file.supprimer();
		assertEquals(1,this.file.getTaille());
		assertSame(enTete,supprime);
	}
	
	//Fonction heuristique Manhattan
	
	//f(noeudx) = f(enTete)
	@Test
	public void testAjouterFilePriorite1() 
	{
		Noeud.setChoixHeuristique(1);
		
		//f(noeud1) = 6 et h(noeud1) = 6 et g(noeud2) = 0
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 6 et h(noeud2) = 6 et g(noeud2) = 0
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(3);
		
		//f(enTete) = 6 et h(enTete) = 1 et g(enTete) = 5 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);

		assertEquals(3,this.file_priorite.getTaille());
		assertSame(enTete,this.file_priorite.getNoeud());

	}
	
	//f(noeudx) > f(enTete)
	@Test
	public void testAjouterFilePriorite2() 
	{
		Noeud.setChoixHeuristique(1);
		
		this.etat.getCaseDeplacement().setX(3);
		this.etat.getCaseDeplacement().setY(0);
		
		//f(noeud1) = 8 et h(noeud1) = 4 et g(noeud1) = 4
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 8 et h(noeud2) = 4 et g(noeud2) = 4
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(2);
		
		//f(enTete) = 6 et h(enTete) = 2 et g(enTete) = 4 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		assertEquals(3,this.file_priorite.getTaille());
		assertSame(enTete,this.file_priorite.getNoeud());
		
	}
	
	//f(noeudx) < f(enTete)
	@Test
	public void testAjouterFilePriorite3() 
	{
		Noeud.setChoixHeuristique(1);
		
		this.etat.getCaseDeplacement().setX(3);
		this.etat.getCaseDeplacement().setY(2);
		
		//f(noeud1) = 6 et h(noeud1) = 2 et g(noeud1) = 4
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 6 et h(noeud2) = 2 et g(noeud1) = 4
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(0);
		
		//f(enTete) = 8 et h(enTete) = 4 et g(enTete) = 4 --> Il n'est pas prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		assertEquals(3,this.file_priorite.getTaille());
		assertNotSame(enTete,this.file_priorite.getNoeud());
		
	}
	
	@Test
	public void testSupprimerFilePriorite() 
	{
		Noeud.setChoixHeuristique(1);
		
		//f(noeud1) = 6 et h(noeud1) = 6 et g(noeud2) = 0
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 6 et h(noeud2) = 6 et g(noeud2) = 0
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(3);
		
		//f(enTete) = 6 et h(enTete) = 1 et g(enTete) = 5 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		Noeud supprime = this.file_priorite.supprimer();
		
		assertEquals(2,this.file_priorite.getTaille());
		assertSame(enTete,supprime);
	}
	
	//Fonction heuristique Euclidienne
	
	//f(noeudx) = f(enTete)
	@Test
	public void testAjouterFilePriorite4() 
	{
		Noeud.setChoixHeuristique(2);
		
		//f(noeud1) = 4.47 et h(noeud1) = 4.47 et g(noeud2) = 0.00
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 4.47 et h(noeud2) = 4.47 et g(noeud2) = 0.00
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(4);
		procheEtatFinal.getCaseDeplacement().setY(3);
		
		//f(enTete) = 4.47 et h(enTete) = 0.00 et g(enTete) = 4.47 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		assertEquals(3,this.file_priorite.getTaille());
		assertSame(enTete,this.file_priorite.getNoeud());

	}
	
	//f(noeudx) > f(enTete)
	@Test
	public void testAjouterFilePriorite5() 
	{
		Noeud.setChoixHeuristique(2);
		
		this.etat.getCaseDeplacement().setX(3);
		this.etat.getCaseDeplacement().setY(0);
		
		//f(noeud1) = 6.32 et h(noeud1) = 3.16 et g(noeud1) = 3.16
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 6.32 et h(noeud2) = 3.16 et g(noeud2) = 3.16
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(2);
		
		//f(enTete) = 4.57 et h(enTete) = 1.41 et g(enTete) = 3.16 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		assertEquals(3,this.file_priorite.getTaille());
		assertSame(enTete,this.file_priorite.getNoeud());
		
	}
	
	//f(noeudx) < f(enTete)
	@Test
	public void testAjouterFilePriorite6() 
	{
		Noeud.setChoixHeuristique(2);
		
		this.etat.getCaseDeplacement().setX(3);
		this.etat.getCaseDeplacement().setY(2);
		
		//f(noeud1) = 4.57 et h(noeud1) = 1.41 et g(noeud1) = 3.16
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
		
		//f(noeud2) = 4.57 et h(noeud2) = 1.41 et g(noeud2) = 3.16
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(3);
		procheEtatFinal.getCaseDeplacement().setY(0);
		
		//f(enTete) = 6.32 et h(enTete) = 3.16 et g(enTete) = 3.16 --> Il n'est pas prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		assertEquals(3,this.file_priorite.getTaille());
		assertNotSame(enTete,this.file_priorite.getNoeud());
		
	}
	
	@Test
	public void testSupprimerFilePriorite2() 
	{
		Noeud.setChoixHeuristique(2);
		
		this.etat.getCaseDeplacement().setX(3);
		this.etat.getCaseDeplacement().setY(0);
		
		//f(noeud1) = 6.32 et h(noeud1) = 3.16 et g(noeud1) = 3.16
		Noeud noeud1 = new Noeud(this.etat,this.parent,"action 1",1,1);
		this.file_priorite.ajouter(noeud1);
				
		//f(noeud2) = 6.32 et h(noeud2) = 3.16 et g(noeud2) = 3.16
		Noeud noeud2 = new Noeud(this.etat,this.parent,"action 2",1,1);
		this.file_priorite.ajouter(noeud2);
		
		Labyrinthe procheEtatFinal = (Labyrinthe)this.etat.cloner();
		procheEtatFinal.getCaseDeplacement().setX(2);
		procheEtatFinal.getCaseDeplacement().setY(0);
		
		//f(enTete) = 5.84 et h(enTete) = 3.60 et g(enTete) = 2.23 --> Proche du but donc prioritaire
		Noeud enTete = new Noeud(procheEtatFinal,this.parent,"action 3",1,1);
		this.file_priorite.ajouter(enTete);
		
		Noeud supprime = this.file_priorite.supprimer();
		
		assertEquals(2,this.file_priorite.getTaille());
		assertSame(enTete,supprime);
	}

}
