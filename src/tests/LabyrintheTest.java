package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import situation.Case;
import situation.CaseException;
import situation.Labyrinthe;
import situation.SituationException;

public class LabyrintheTest {

	private ArrayList<Case> casesLabyrintheInitiale;
	private ArrayList<Case> casesLabyrintheFinale;
	private Case entree;
	private Case sortie;
	private Case deplacementInitiale;
	private Case deplacementFinale;
	private Labyrinthe labyrintheInitiale;
	private Labyrinthe labyrintheFinale;
	
	@Before
	public void setUp()
	{
		try
		{
			this.casesLabyrintheInitiale = new ArrayList<Case>();
			this.casesLabyrintheFinale = new ArrayList<Case>();
			for(int y=0 ; y<6 ; y++)
			{
				for(int x=0 ; x<5 ; x++)
				{
					if((x==0 && y==2) || (x==0 && y==3) || (x==2 && y==1) || 
					   (x==2 && y==2) || (x==2 && y==3) || (x==2 && y==4) || 
					   (x==3 && y==4) || (x==4 && y==1) || (x==4 && y==2))
					{
						this.casesLabyrintheInitiale.add(new Case(x,y,false,"M"));
						this.casesLabyrintheFinale.add(new Case(x,y,false,"M"));
					}
					else if((x==0 && y==1))
					{
						this.casesLabyrintheInitiale.add(new Case(x,y,true,"E/D"));
						this.casesLabyrintheFinale.add(new Case(x,y,true,"E"));
					}
					else if((x==4 && y==3))
					{
						this.casesLabyrintheInitiale.add(new Case(x,y,true,"S"));
						this.casesLabyrintheFinale.add(new Case(x,y,false,"S/D"));
					}
					else
					{
						this.casesLabyrintheInitiale.add(new Case(x,y,true," "));
						this.casesLabyrintheFinale.add(new Case(x,y,true," "));
					}
				}
			}

			this.entree = new Case(0,1,true,"E");
			this.deplacementInitiale = new Case(0,1,true,"D");
			this.deplacementFinale = new Case(4,3,true,"D");
			this.sortie = new Case(4,3,true,"S");
			
			this.labyrintheInitiale = new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementInitiale,5,6);
			this.labyrintheFinale = new Labyrinthe(this.casesLabyrintheFinale,this.entree,this.sortie,this.deplacementFinale,5,6);
		}
		
		catch(CaseException e)
		{
			System.out.println(e.getMessage());
		}
		catch(SituationException e)
		{
			System.out.println(e.getMessage());
		}
	}

	@After
	public void tearDown()
	{
		this.casesLabyrintheInitiale = null;
		this.casesLabyrintheFinale = null;
		this.entree = null;
		this.deplacementFinale = null;
		this.deplacementInitiale = null;
		this.sortie = null;
		this.labyrintheInitiale = null;
		this.labyrintheFinale = null;
	}
	
	@Test(expected=SituationException.class)
	public void testLabyrinthe1() throws SituationException
	{
		new Labyrinthe(null,null,null,null,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,null,null,null,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,null,null,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,null,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementInitiale,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementFinale,0,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementInitiale,1,0);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementFinale,0,1);
		new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.entree,this.deplacementFinale,0,1);
		new Labyrinthe(this.casesLabyrintheInitiale,this.sortie,this.sortie,this.deplacementFinale,0,1);
	}
	
	@Test
	public void testLabyrinthe2()
	{
		boolean creer;
		try
		{
			new Labyrinthe(this.casesLabyrintheInitiale,this.entree,this.sortie,this.deplacementInitiale,5,6);
			new Labyrinthe(this.casesLabyrintheFinale,this.entree,this.sortie,this.deplacementFinale,5,6);
			creer = true;
		}
		catch(SituationException e)
		{
			creer = false;
		}
		assertTrue(creer);
	}
	
	@Test
	public void testCloner() 
	{
		Labyrinthe labyrintheClone = (Labyrinthe)this.labyrintheInitiale.cloner();
		//L'objet cloné n'a pas la même référence
		assertNotSame(this.labyrintheInitiale,labyrintheClone);
		//Mais ils sont bien égaux
		assertEquals(this.labyrintheInitiale,labyrintheClone);
	}

	@Test
	public void testGetCases() 
	{
		assertSame(this.casesLabyrintheInitiale,this.labyrintheInitiale.getCases());
		assertSame(this.casesLabyrintheFinale,this.labyrintheFinale.getCases());
	}

	@Test
	public void testgetCaseEntree() 
	{
		assertSame(this.entree,this.labyrintheInitiale.getCaseEntree());
		assertSame(this.entree,this.labyrintheFinale.getCaseEntree());
	}

	@Test
	public void testGetCaseSortie() 
	{
		assertSame(this.sortie,this.labyrintheInitiale.getCaseSortie());
		assertSame(this.sortie,this.labyrintheFinale.getCaseSortie());
	}
	
	@Test
	public void testGetCaseDeplacement() 
	{
		assertSame(this.deplacementInitiale,this.labyrintheInitiale.getCaseDeplacement());
		assertSame(this.deplacementFinale,this.labyrintheFinale.getCaseDeplacement());
	}

	@Test
	public void testGetX() 
	{
		assertEquals(5,labyrintheInitiale.getX());
		assertEquals(5,labyrintheFinale.getX());
	}

	@Test
	public void testGetY() 
	{
		assertEquals(6,labyrintheInitiale.getY());
		assertEquals(6,labyrintheFinale.getY());
	}

	@Test
	public void testGetCase1() 
	{
		Case c = this.labyrintheInitiale.getCase(2, 2);
		assertNotNull(c);
	}
	
	@Test
	public void testGetCase2() 
	{
		Case c = this.labyrintheInitiale.getCase(20, 20);
		assertNull(c);
	}

	@Test
	public void testEqualsObject1() 
	{
		assertFalse(this.labyrintheInitiale.equals(this.labyrintheFinale));
		assertFalse(this.labyrintheFinale.equals(this.labyrintheInitiale));
	}
	
	@Test
	public void testEqualsObject2() 
	{
		this.labyrintheInitiale.getCaseDeplacement().setX(4);
		this.labyrintheInitiale.getCaseDeplacement().setY(3);
		assertTrue(this.labyrintheInitiale.equals(this.labyrintheFinale));
	}
	
	@Test
	public void testVerifierOperateurs1() {
		assertTrue(this.labyrintheInitiale.verifierOperateurs());
	}

	//Vérifier déplacement vers le haut
	
	@Test
	public void testVerifierOperateursInt1() {
		assertTrue("Vers une case vide",this.labyrintheInitiale.verifierOperateurs(1));
	}
	
	@Test
	public void testVerifierOperateursInt2() {
		this.labyrintheInitiale.getCaseDeplacement().setX(0);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertFalse("Hors cadre du jeu",this.labyrintheInitiale.verifierOperateurs(1));
	}
	
	@Test
	public void testVerifierOperateursInt3() {
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(5);
		assertFalse("Vers un mur",this.labyrintheInitiale.verifierOperateurs(1));
	}
	
	//Effectuer déplacement vers le haut
	
	@Test
	public void testAppliquerOperateurs1() {
		String operateur = this.labyrintheInitiale.appliquerOperateurs(1);
		assertEquals("Vers une case vide","Déplacement vers le haut",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs2() {
		this.labyrintheInitiale.getCaseDeplacement().setX(0);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(1);
		assertEquals("Vers une case vide","Déplacement vers le haut impossible",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs3() {
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(5);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(1);
		assertEquals("Vers un mur","Déplacement vers le haut impossible",operateur);
		assertEquals(2,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(5,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
	
	//Vérifier déplacement vers le bas
	
	@Test
	public void testVerifierOperateursInt4() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		assertTrue("Vers une case vide",this.labyrintheInitiale.verifierOperateurs(2));
	}
	
	@Test
	public void testVerifierOperateursInt5() {
		this.labyrintheInitiale.getCaseDeplacement().setX(0);
		this.labyrintheInitiale.getCaseDeplacement().setY(5);
		assertFalse("Hors cadre du jeu",this.labyrintheInitiale.verifierOperateurs(2));
	}
	
	@Test
	public void testVerifierOperateursInt6() {
		assertFalse("Vers un mur",this.labyrintheInitiale.verifierOperateurs(2));
	}
	
	//Effectuer déplacement vers le bas
	
	@Test
	public void testAppliquerOperateurs4() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(2);
		assertEquals("Vers une case vide","Déplacement vers le bas",operateur);
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(2,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs5() {
		this.labyrintheInitiale.getCaseDeplacement().setX(0);
		this.labyrintheInitiale.getCaseDeplacement().setY(5);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(2);
		assertEquals("Hors cadre du jeu","Déplacement vers le bas impossible",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(5,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs6() {
		String operateur = this.labyrintheInitiale.appliquerOperateurs(2);
		assertEquals("Vers un mur","Déplacement vers le bas impossible",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
	
	//Vérifier déplacement vers la gauche
	
	@Test
	public void testVerifierOperateursInt7() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		assertTrue("Vers une case vide",this.labyrintheInitiale.verifierOperateurs(3));
	}
	
	@Test
	public void testVerifierOperateursInt8() {
		assertFalse("Hors cadre du jeu",this.labyrintheInitiale.verifierOperateurs(3));
	}
	
	@Test
	public void testVerifierOperateursInt9() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(2);
		assertFalse("Vers un mur",this.labyrintheInitiale.verifierOperateurs(3));
	}
	
	//Effectuer déplacement vers la gauche
	
	@Test
	public void testAppliquerOperateurs7() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(3);
		assertEquals("Vers une case vide","Déplacement vers la gauche",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs8() {
		String operateur = this.labyrintheInitiale.appliquerOperateurs(3);
		assertEquals("Hors cadre du jeu","Déplacement vers la gauche impossible",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs9() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(2);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(3);
		assertEquals("Vers un mur","Déplacement vers la gauche impossible",operateur);
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(2,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
	
	//Vérifier déplacement vers la droite
	
	@Test
	public void testVerifierOperateursInt10() {
		assertTrue("Vers une case vide",this.labyrintheInitiale.verifierOperateurs(4));
	}
	
	@Test
	public void testVerifierOperateursInt11() {
		this.labyrintheInitiale.getCaseDeplacement().setX(4);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertFalse("Hors cadre du jeu",this.labyrintheInitiale.verifierOperateurs(4));
	}
	
	@Test
	public void testVerifierOperateursInt12() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		assertFalse("Vers un mur",this.labyrintheInitiale.verifierOperateurs(4));
	}
	
	//Effectuer déplacement vers la droite
	
	@Test
	public void testAppliquerOperateurs10() {
		String operateur = this.labyrintheInitiale.appliquerOperateurs(4);
		assertEquals("Vers une case vide","Déplacement vers la droite",operateur);
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs11() {
		this.labyrintheInitiale.getCaseDeplacement().setX(4);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(4);
		assertEquals("Hors cadre du jeu","Déplacement vers la droite impossible",operateur);
		assertEquals(4,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
		
	@Test
	public void testAppliquerOperateurs12() {
		this.labyrintheInitiale.getCaseDeplacement().setX(1);
		this.labyrintheInitiale.getCaseDeplacement().setY(1);
		String operateur = this.labyrintheInitiale.appliquerOperateurs(4);
		assertEquals("Vers un mur","Déplacement vers la droite impossible",operateur);
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
	
	//Vérifier un déplacement qui n'existe pas
	@Test
	public void testVerifierOperateursInt13() {
		assertFalse("Vers une case inexistante",this.labyrintheInitiale.verifierOperateurs(10));
	}
	
	//Effectuer un déplacement qui n'existe pas
	@Test
	public void testAppliquerOperateurs13() {
		String operateur = this.labyrintheInitiale.appliquerOperateurs(10);
		assertEquals("Vers une case inexistant","Opérateur inconnu",operateur);
		assertEquals(0,this.labyrintheInitiale.getCaseDeplacement().getX());
		assertEquals(1,this.labyrintheInitiale.getCaseDeplacement().getY());
	}
	
	//Les fonctions heuristiques
	
	//Fonction de Manhattan
	
	//Avec h(n)
	@Test
	public void testCalculerHHeuristiqueInt1(){
		//nCourant = (xa=0,ya=1) ; nFinale = (xb=4,yb=3)
		//h(nCourant) = abs(xb-xa) + abs(yb-ya) = (4-0) + (3-1) = 6;
		assertEquals(6.0,this.labyrintheInitiale.calculerHHeuristique(1),0.1);
	}
	
	//Avec g(n)
	@Test
	public void testCalculerGHeuristiqueInt1(){
		//nCourant = (xa=2,ya=0) ; nInitiale = (xb=0,yb=1)
		//g(nCourant) = abs(xb-xa) + abs(yb-ya) = (0-2) + (1-0) = 3;
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertEquals(3.0,this.labyrintheInitiale.calculerGHeuristique(1),0.1);
	}
	
	//Avec f(n)
	@Test
	public void testCalculerFHeuristiqueInt1(){
		//nCourant = (xa=2,ya=0) ; n'Initiale = (xb=0,yb=1) ; nFinale = (xc=4,yc=3)
		//h(nCourant) = abs(xc-xa) + abs(yc-ya) = (4-2) + (3-0) = 5;
		//g(nCourant) = abs(xb-xa) + abs(yb-ya) = (0-2) + (1-0) = 3;
		//f(nCourant) = h(nCourant) + g(nCourant) = 5 + 3 = 8
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertEquals(8.0,this.labyrintheInitiale.calculerFHeuristique(1),0.1);
	}
	
	//Fonction euclidienne
	
	//Avec h(n)
	@Test
	public void testCalculerHHeuristiqueInt2(){
		//nCourant = (xa=0,ya=1) ; nFinale = (xb=4,yb=3)
		//h(nCourant) = sqrt(abs(xb-xa) au carré + abs(yb-ya) au carré) = sqrt((4-0) + (3-1)) = sqrt(16 + 4) = 4.47
		assertEquals(4.47,this.labyrintheInitiale.calculerHHeuristique(2),0.1);
	}
		
	//Avec g(n)
	@Test
	public void testCalculerGHeuristiqueInt2(){
		//nCourant = (xa=2,ya=0) ; nInitiale = (xb=0,yb=1)
		//g(nCourant) = sqrt(abs(xb-xa) au carré + abs(yb-ya) au carré) = sqrt((0-2) + (1-0)) = 2.23;
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertEquals(2.23,this.labyrintheInitiale.calculerGHeuristique(2),0.1);
	}
		
	//Avec f(n)
	@Test
	public void testCalculerFHeuristiqueInt2(){
		//nCourant = (xa=2,ya=0) ; nInitiale = (xb=0,yb=1) ; nFinale = (xc=4,yc=3)
		//h(nCourant) = sqrt(abs(xc-xa) au carré + abs(yc-ya) au carré) = sqrt((4-2) + (3-0)) = 3.60;
		//g(nCourant) = sqrt(abs(xb-xa) au carré + abs(yb-ya) au carré) = sqrt((0-2) + (1-0)) = 2.24;
		//f(nCourant) = h(nCourant) + g(nCourant) = 3 + 3 = 5.84
		this.labyrintheInitiale.getCaseDeplacement().setX(2);
		this.labyrintheInitiale.getCaseDeplacement().setY(0);
		assertEquals(5.84,this.labyrintheInitiale.calculerFHeuristique(2),0.1);
	}

}
