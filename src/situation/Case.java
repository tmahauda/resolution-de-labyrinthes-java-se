package situation;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Case représente une case d'un jeu (labyrinthe, tablut, etc)
*/
public class Case
{
	/**
     * x représente la coordoonée des abscisses
     */
	private int x;
	
	/**
     * y représente la coordoonée des ordonnées
     */
	private int y;
	
	/**
     * estVide représente une case vide ou non (mur pour le labyrinthe)
     */
	private boolean estVide;
	
	/**
     * contenu représente des caractères à afficher
     */
	private String contenu;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon une condition :
     * - Si les coordonnées x et y ne sont pas positives, alors la case n'est pas valide
     * @param estVide pour représenter une case vide (ou un mur pour le labyrinthe)
     * @param contenu le contenu de la case
     * @param x la coordonnée abscisse
     * @param y la coordonnée ordonnée
     * @see CaseException
     * @throws CaseException Si la condition n'est pas respectée
     */
	public Case(int x, int y, boolean estVide, String contenu) throws CaseException
	{
		if(x>=0 && y>=0)
		{
			this.x = x;
			this.y = y;
			this.estVide = estVide;
			this.contenu = contenu;
		}
		else throw new CaseException("Les coordonnées de la case ne sont pas valide");
	}
	
    /**
	 * Méthode qui permet de cloner une case
	 * @return une nouvelle case
     */
	public Case clone()
	{
		Case caseClone = null;
		try
		{
			caseClone = new Case(this.x,this.y,this.estVide,this.contenu);
		}
		catch(CaseException e) {}
		return caseClone;
	}
	
	/**
     * Sélecteur qui permet de récupérer la coordonnée x
     * @return x
     */
	public int getX()
	{
		return this.x;
	}
	
	/**
     * Modificateur qui permet de modifier la coordonnée x
     * @param x la coordonnée abscisse
     */
	public void setX(int x)
	{
		this.x = x;
	}
	
	/**
     * Sélecteur qui permet de récupérer la coordonnée y
     * @return y la coordonnée ordonné
     */
	public int getY()
	{
		return this.y;
	}
	
	/**
     * Modificateur qui permet de modifier la coordonnée y
     * @param y la coordonnée à modifier
     */
	public void setY(int y)
	{
		this.y = y;
	}
	
	/**
     * Sélecteur qui permet de savoir si la case est vide (mur) ou non
     * @return true ou false
     */
	public boolean getEstVide()
	{
		return this.estVide;
	}
	
	/**
     * Modificateur qui permet de modifier le role de la case : soit vide (mur) ou non
     * @param estVide true ou false
     */
	public void setEstVide(boolean estVide)
	{
		this.estVide = estVide;
	}
	
	/**
     * Sélecteur qui permet de récupérer le contenu de la case
     * @return contenu
     */
	public String getContenu()
	{
		return this.contenu;
	}
	
	/**
     * Modificateur qui permet de modifier le contenu de la case
     * @param contenu le contenu de la case
     */
	public void setContenu(String contenu)
	{
		this.contenu = contenu;
	}
	
	@Override
	public String toString()
	{
		NumberFormat format = new DecimalFormat("00");
		return "("+format.format(this.x)+";"+format.format(this.y)+") "+ this.contenu;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Case))
			return false;
		Case other = (Case) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
}
