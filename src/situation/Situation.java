package situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* L'interface Situation représente une situation d'un problème à résoudre. Elle doit être implémentée
* et redéfinir toutes les méthodes pour être utilisée dans les algorithmes de recherche
*/
public interface Situation
{
	/**
     * Permet de vérifier si au moins un opérateur est applicable pour générer des noeuds
     * @return true ou false
     */
	public boolean verifierOperateurs();
	
	/**
     * Permet de vérifier si un opérateur choisi par son index "choix" est applicable ou non
     * @param choix pour vérifier un opérateur
     * @return true ou false
     */
	public boolean verifierOperateurs(int choix);
	
	/**
     * Permet d'appliquer un opérateur choisi par son index "choix"
     * @param choix pour appliquer un opérateur
     * @return le nom de l'opérateur qui aura été choisi
     */
	public String appliquerOperateurs(int choix);
	
	/**
     * Permet de cloner une situation pour générer des noeuds
     * @see Situation
     * @return une nouvelle situation
     */
	public Situation cloner();
	
	/**
     * Permet de calculer la fonction heuristique h(n) 
     * @param choix pour choisir la fonction heuristique h
     * @return le cout estimé du meilleur chemin du noeud n au noeud but
     */
	public double calculerHHeuristique(int choix);
	
	/**
     * Permet de calculer la fonction heuristique g(n)
     * @param choix pour choisir la fonction heuristique g 
     * @return le cout exact du chemin entre le noeud initial et le noeud n
     */
	public double calculerGHeuristique(int choix);
	
	/**
     * Permet de calculer la fonction heuristique f(n)
     * @param choix pour choisir la fonction heuristique f 
     * @return le cout estimé du meilleur chemin passant par n
     */
	public double calculerFHeuristique(int choix);
}
