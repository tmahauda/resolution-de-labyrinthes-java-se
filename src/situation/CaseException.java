package situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe CaseException permet de lancer une exception relative à la classe Case
* @see Case
*/
@SuppressWarnings("serial")
public class CaseException extends Exception
{
	public CaseException(String message)
	{
		super(message);
	}
}
