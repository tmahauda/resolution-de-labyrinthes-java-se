package situation;
import java.util.ArrayList;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Labyrinthe représente une situation potentiellement résoulable
* @see Situation
*/
public class Labyrinthe implements Situation
{
	private ArrayList<Case> cases;
	private Case entree;
	private Case sortie;
	private Case deplacement;
	private int x;
	private int y;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon une condition :
     * - Si les cases sont vides et que les coordonnées ne sont pas positives, alors le labyrinthe ne peut pas être crée
     * @param cases les cases du labyrinthe
     * @param entree le point de départ
     * @param sortie le point de sortie
     * @param deplacement le point qui se déplace dans le labyrinthe
     * @param x la coordonnée abscisse
     * @param y la coordonnée ordonnée
     * @see SituationException
     * @throws SituationException Si la condition n'est pas respectée
     */
	public Labyrinthe(ArrayList<Case> cases, Case entree, Case sortie, Case deplacement, int x, int y) throws SituationException 
	{
		if(cases != null && !cases.isEmpty() && entree != null && sortie != null && deplacement !=null 
			&& x>0 && y>0 && !entree.equals(sortie))
		{
			this.cases = cases;
			this.entree = entree;
			this.sortie = sortie;
			this.deplacement = deplacement;
			this.x = x;
			this.y = y;
		}
		else throw new SituationException("Le labyrinthe ne peut pas être crée");
	}
	
    /**
	 * @see Situation#cloner()
     */
	public Situation cloner()
	{
		ArrayList<Case> casesClone = new ArrayList<Case>();
		Labyrinthe labyrintheClone = null;
		for(Case c : this.cases)
		{
			casesClone.add(c.clone());
		}
		
		try
		{
			labyrintheClone = new Labyrinthe(casesClone, this.entree.clone(), this.sortie.clone(), this.deplacement.clone(), this.x, this.y);
		}
		catch(SituationException e){}
		
		return labyrintheClone;
	}
	
	public ArrayList<Case> getCases()
	{
		return this.cases;
	}
	
	public Case getCaseEntree()
	{
		return this.entree;
	}
	
	public Case getCaseSortie()
	{
		return this.sortie;
	}
	
	public Case getCaseDeplacement()
	{
		return this.deplacement;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}

	/**
	* Méthode qui permet de récupérer une case d'un labyrinthe selon les coordonnées :
	* @param x la coordonnée abscisse
	* @param y la coordonnée ordonnée
	* @return la case selon les coordonnées x et y
	*/
	public Case getCase(int x, int y)
	{
		Case getCase = null;
		for(Case c : this.cases)
		{
			if(c.getX() == x && c.getY() == y)
			{
				getCase = c;
			}
		}
		return getCase;
	}
	
	@Override
	public String toString()
	{
		String toString = "";
		int x = 0;
		
		for(Case c : this.cases)
		{
			toString = toString + " | " + c.toString();
			x++;
			if(x==this.x)
			{
				toString = toString + "\n";
				x = 0;
			}
		}
		
		toString = toString + "Entrée = " + this.entree.toString() + ", Sortie = " + this.sortie.toString() + ", Déplacement = " + this.deplacement.toString();
		
		return toString;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (obj == null)
			return false;
		if (!(obj instanceof Labyrinthe))
			return false;
		Labyrinthe other = (Labyrinthe)obj;
		if(this.deplacement.equals(other.getCaseDeplacement()))
		{
			return true;
		}
		else return false;
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case de déplacement peut aller en haut selon une condition :
	* - Si la case de déplacement peut susceptiblement dépassé le cadre du jeu ou que la case n'est pas vide, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerHaut()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		return (y>0 && this.getCase(x, y-1).getEstVide());
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement haut
	*/
	private void effectuerDeplacerHaut()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		this.getCase(x, y).setContenu(" x ");
		y--;
		this.deplacement.setY(y);
		this.getCase(x, y).setContenu(" D ");
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case de déplacement peut aller en bas selon une condition :
	* - Si la case de déplacement peut susceptiblement dépassé le cadre du jeu ou que la case n'est pas vide, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerBas()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		return (y<this.y-1 && this.getCase(x, y+1).getEstVide());
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement bas
	*/
	private void effectuerDeplacerBas()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		this.getCase(x, y).setContenu(" x ");
		y++;
		this.deplacement.setY(y);
		this.getCase(x, y).setContenu(" D ");
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case de déplacement peut aller à gauche selon une condition :
	* - Si la case de déplacement peut susceptiblement dépassé le cadre du jeu ou que la case n'est pas vide, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerGauche()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		return (x>0 && this.getCase(x-1, y).getEstVide());
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement gauche
	*/
	private void effectuerDeplacerGauche()
	{
		int x = this.deplacement.getX();
		int y = this.deplacement.getY();
		this.getCase(x, y).setContenu(" x ");
		x--;
		this.deplacement.setX(x);
		this.getCase(x, y).setContenu(" D ");
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case de déplacement peut aller à droite selon une condition :
	* - Si la case de déplacement peut susceptiblement dépassé le cadre du jeu ou que la case n'est pas vide, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerDroite()
	{
		int y = this.deplacement.getY();
		int x = this.deplacement.getX();
		return (x<this.x-1 && this.getCase(x+1, y).getEstVide());
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement droit
	*/
	private void effectuerDeplacerDroite()
	{
		int x = this.deplacement.getX();
		int y = this.deplacement.getY();
		this.getCase(x, y).setContenu(" x ");
		x++;
		this.deplacement.setX(x);
		this.getCase(x, y).setContenu(" D ");
	}
	
    /**
	 * @see Situation#verifierOperateurs()
     */
	public boolean verifierOperateurs()
	{
		return verifierDeplacerHaut() || verifierDeplacerBas() || verifierDeplacerGauche() || verifierDeplacerDroite();
	}
	
    /**
	 * @see Situation#verifierOperateurs(int)
     */
	public boolean verifierOperateurs(int choix)
	{
		boolean verifier = false;
		switch(choix)
		{
		  case 1:
		    verifier = this.verifierDeplacerHaut();
		    break;
		  case 2:
			verifier = this.verifierDeplacerBas();
			break;
		  case 3:
			verifier = this.verifierDeplacerGauche();
			break;
		  case 4:
			verifier = this.verifierDeplacerDroite();
			break;
		  default:
		    verifier = false;             
		}
		return verifier;
	}
	
    /**
	 * @see Situation#appliquerOperateurs(int)
     */
	public String appliquerOperateurs(int choix)
	{
		switch(choix)
		{
		  case 1:
			  if(this.verifierDeplacerHaut())
			  {
				 this.effectuerDeplacerHaut();
				 return "Déplacement vers le haut";
			  }
			  else return "Déplacement vers le haut impossible";
		  case 2:
			  if(this.verifierDeplacerBas())
			  {
				 this.effectuerDeplacerBas();
				 return "Déplacement vers le bas";
			  }
			  else return "Déplacement vers le bas impossible";
		  case 3:
			  if(this.verifierDeplacerGauche())
			  {
				 this.effectuerDeplacerGauche();
				 return "Déplacement vers la gauche";
			  }
			  else return "Déplacement vers la gauche impossible";
		  case 4:
			  if(this.verifierDeplacerDroite())
			  {
				 this.effectuerDeplacerDroite();
				 return "Déplacement vers la droite";
			  }
			  else return "Déplacement vers la droite impossible";
		  default:
			return "Opérateur inconnu";
		}
	}
	
    /**
	 * @see Situation#calculerHHeuristique(int)
     */
	public double calculerHHeuristique(int choix)
	{
		switch(choix)
		{
		 case 1:
			return calculerHManhattan();
		 case 2:
			return calculerHEuclidienne();
		 default:
			return 0;
		}
	}
	
    /**
	 * @see Situation#calculerGHeuristique(int)
     */
	public double calculerGHeuristique(int choix)
	{
		switch(choix)
		{
		 case 1:
			return calculerGManhattan();
		 case 2:
			return calculerGEuclidienne();
		 default:
			return 0;
		}
	}
	
    /**
	 * @see Situation#calculerFHeuristique(int)
     */
	public double calculerFHeuristique(int choix)
	{
		switch(choix)
		{
		 case 1:
			return calculerFManhattan();
		 case 2:
			return calculerFEuclidienne();
		 default:
			return 0;
		}
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique h(n) de Manhattan
	 * @return le résultat
     */
	private double calculerHManhattan()
	{
		int xb = this.sortie.getX();
		int xa = this.deplacement.getX();
		int yb = this.sortie.getY();
		int ya = this.deplacement.getY();
		
		return Math.abs(xb-xa) + Math.abs(yb-ya);
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique g(n) de Manhattan
	 * @return le résultat
     */
	private double calculerGManhattan()
	{
		int xb = this.deplacement.getX();
		int xa = this.entree.getX();
		int yb = this.deplacement.getY();
		int ya = this.entree.getY();
		
		return Math.abs(xb-xa) + Math.abs(yb-ya);
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique f(n) de Manhattan
	 * @return le résultat
     */
	private double calculerFManhattan()
	{
		return calculerGManhattan()+calculerHManhattan();
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique h(n) Euclidienne
	 * @return le résultat
     */
	private double calculerHEuclidienne()
	{
		int xb = this.sortie.getX();
		int xa = this.deplacement.getX();
		int yb = this.sortie.getY();
		int ya = this.deplacement.getY();
		
		return Math.sqrt(Math.pow((xb-xa),2) + Math.pow((yb-ya),2));
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique g(n) Euclidienne
	 * @return le résultat
     */
	private double calculerGEuclidienne()
	{
		int xb = this.deplacement.getX();
		int xa = this.entree.getX();
		int yb = this.deplacement.getY();
		int ya = this.entree.getY();
		
		return Math.sqrt(Math.pow((xb-xa),2) + Math.pow((yb-ya),2));
	}
	
    /**
	 * Méthode privée qui permet de calculer la fonction heuristique f(n) Euclidienne
	 * @return le résultat
     */
	private double calculerFEuclidienne()
	{
		return calculerGEuclidienne()+calculerHEuclidienne();
	}
	
	
}
