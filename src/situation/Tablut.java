package situation;
import java.util.ArrayList;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Tablut représente une situation potentiellement résoulable
* @see Situation
*/

public class Tablut implements Situation
{
	private ArrayList<Case> cases;
	private int x;
	private int y;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon une condition :
     * - Si les cases sont vides et que les coordonnées ne sont pas positives, alors le tablut ne peut pas être crée
     * @param cases les cases du tablut
     * @param x la coordonnée abscisse
     * @param y la coordonnée ordonnée
     * @see SituationException
     * @throws SituationException Si la condition n'est pas respectée
     */
	public Tablut(ArrayList<Case> cases, int x, int y) throws SituationException  
	{
		if(cases != null && !cases.isEmpty() && x>0 && y>0)
		{
			this.cases = cases;
			this.x = x;
			this.y = y;
		}
		else throw new SituationException("Le tablut ne peut pas être crée");
	}
	
    /**
	 * @see Situation#cloner()
     */
	public Situation cloner() 
	{
		ArrayList<Case> casesClone = new ArrayList<Case>();
		Tablut tablutClone = null;
		for(Case c : this.cases)
		{
			casesClone.add(c.clone());
		}
		
		try
		{
			tablutClone = new Tablut(casesClone,this.x,this.y);
		}
		catch(SituationException e){}
		
		return tablutClone;
	}
	
	/**
	* Méthode qui permet de récupérer une case d'un tablut selon les coordonnées :
	* @param x la coordonnée abscisse
	* @param y la coordonnée ordonnée
	* @return la case selon les coordonnées
	*/
	public Case getCase(int x, int y)
	{
		Case getCase = null;
		for(Case c : this.cases)
		{
			if(c.getX() == x && c.getY() == y)
			{
				getCase = c;
			}
		}
		return getCase;
	}
	
	/**
	* Méthode qui permet de récupérer la case vide du tablut
	* @return la case vide
	*/
	public Case getCaseVide()
	{
		Case getCase = null;
		for(Case c : this.cases)
		{
			if(c.getEstVide())
			{
				getCase = c;
			}
		}
		return getCase;
	}
	
	@Override
	public String toString()
	{
		String toString = "";
		int x = 0;
		
		for(Case c : this.cases)
		{
			toString = toString + " | " + c.toString();
			x++;
			if(x==this.x)
			{
				toString = toString + "\n";
				x = 0;
			}
		}
		
		return toString;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (obj == null)
			return false;
		if (!(obj instanceof Tablut))
			return false;
		Tablut other = (Tablut)obj;
		if(this.toString().equals(other.toString()))
		{
			return true;
		}
		else return false;
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case vide peut aller en haut selon une condition :
	* - Si la case vide peut susceptiblement dépassé le cadre du jeu, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerHaut()
	{
		int y = this.getCaseVide().getY();
		return y>0;
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement haut
	*/
	private void effectuerDeplacerHaut()
	{
		Case vide = this.getCaseVide();
		Case haut = this.getCase(vide.getX(), vide.getY()-1);
		String contenu = haut.getContenu();
		vide.setContenu(contenu);
		vide.setEstVide(false);
		haut.setContenu(" ");
		haut.setEstVide(true);
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case vide peut aller en bas selon une condition :
	* - Si la case vide peut susceptiblement dépassé le cadre du jeu, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerBas()
	{
		int y = this.getCaseVide().getY();
		return y<this.y-1;
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement bas
	*/
	private void effectuerDeplacerBas()
	{
		Case vide = this.getCaseVide();
		Case haut = this.getCase(vide.getX(), vide.getY()+1);
		String contenu = haut.getContenu();
		vide.setContenu(contenu);
		vide.setEstVide(false);
		haut.setContenu(" ");
		haut.setEstVide(true);
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case vide peut aller à gauche selon une condition :
	* - Si la case vide peut susceptiblement dépassé le cadre du jeu, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerGauche()
	{
		int x = this.getCaseVide().getX();
		return x>0;
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement gauche
	*/
	private void effectuerDeplacerGauche()
	{
		Case vide = this.getCaseVide();
		Case haut = this.getCase(vide.getX()-1, vide.getY());
		String contenu = haut.getContenu();
		vide.setContenu(contenu);
		vide.setEstVide(false);
		haut.setContenu(" ");
		haut.setEstVide(true);
	}
	
	/**
	* Méthode privée qui permet de vérifier si la case vide peut aller à droite selon une condition :
	* - Si la case vide peut susceptiblement dépassé le cadre du jeu, alors on ne peut pas la déplacer
	* @return true ou false
	*/
	private boolean verifierDeplacerDroite()
	{
		int x = this.getCaseVide().getX();
		return x<this.x-1;
	}
	
	/**
	* Méthode privée qui permet d'appliquer l'opérateur déplacement droite
	*/
	private void effectuerDeplacerDroite()
	{
		Case vide = this.getCaseVide();
		Case haut = this.getCase(vide.getX()+1, vide.getY());
		String contenu = haut.getContenu();
		vide.setContenu(contenu);
		vide.setEstVide(false);
		haut.setContenu(" ");
		haut.setEstVide(true);
	}
	
    /**
	 * @see Situation#verifierOperateurs()
     */
	public boolean verifierOperateurs()
	{
		return verifierDeplacerHaut() || verifierDeplacerBas() || verifierDeplacerGauche() || verifierDeplacerDroite();
	}
	
    /**
	 * @see Situation#verifierOperateurs(int)
     */
	public boolean verifierOperateurs(int choix)
	{
		switch(choix)
		{
		  case 1:
		    return this.verifierDeplacerHaut();
		  case 2:
			return this.verifierDeplacerBas();
		  case 3:
			return this.verifierDeplacerGauche();
		  case 4:
			return this.verifierDeplacerDroite();
		  default:
		    return false;             
		}
	}
	
    /**
	 * @see Situation#appliquerOperateurs(int)
     */
	public String appliquerOperateurs(int choix)
	{
		switch(choix)
		{
		  case 1:
			  if(this.verifierDeplacerHaut())
			  {
				 this.effectuerDeplacerHaut();
				 return "Déplacement vers le haut";
			  }
			  else return "Déplacement vers le haut impossible";
		  case 2:
			  if(this.verifierDeplacerBas())
			  {
				 this.effectuerDeplacerBas();
				 return "Déplacement vers le bas";
			  }
			  else return "Déplacement vers le bas impossible";
		  case 3:
			  if(this.verifierDeplacerGauche())
			  {
				 this.effectuerDeplacerGauche();
				 return "Déplacement vers la gauche";
			  }
			  else return "Déplacement vers la gauche impossible";
		  case 4:
			  if(this.verifierDeplacerDroite())
			  {
				 this.effectuerDeplacerDroite();
				 return "Déplacement vers la droite";
			  }
			  else return "Déplacement vers la droite impossible";
		  default:
			return "Opérateur inconnu";
		}
	}
	
	/**
	* @see Situation#calculerHHeuristique(int)
	*/
	public double calculerHHeuristique(int choix)
	{
		return 0;
	}
	
	 /**
	 * @see Situation#calculerGHeuristique(int)
	 */
	public double calculerGHeuristique(int choix)
	{
		return 0;
	}
	
	/**
    * @see Situation#calculerFHeuristique(int)
	*/
	public double calculerFHeuristique(int choix)
	{
		return calculerHHeuristique(choix)+calculerGHeuristique(choix);
	}
}
