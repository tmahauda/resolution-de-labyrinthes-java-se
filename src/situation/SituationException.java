package situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe SituationException permet de lancer une exception relative à la classe Situation
* @see Situation
*/
@SuppressWarnings("serial")
public class SituationException extends Exception
{
	public SituationException(String message)
	{
		super(message);
	}
}
