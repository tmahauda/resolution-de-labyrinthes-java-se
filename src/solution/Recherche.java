package solution;
import probleme.*;
import situation.Situation;

import java.util.ArrayList;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe static Recherche permet d'explorer en arbre le probleme pour trouver la solution
*/
public class Recherche
{
	/**
     * Constructeur privée qui évite l'instanciation en objet. Cette classe comporte uniquement des méthodes de classes
     */
	private Recherche(){}
	
	/**
     * Méthode de classe principale qui permet d'explorer en arbre son problème grâce à une stratégie mis en place
     * pour trouver la solution et l'écrire dans un fichier, stocké dans le répertoire Solution
     * @see Recherche#explorer_en_graphe_profondeur(Probleme, Strategie)
     * @see Recherche#explorer_en_graphe_largeur(Probleme, Strategie)
     * @see Recherche#explorer_en_graphe_A_Heuristique_Manhattan(Probleme, Strategie)
     * @see Recherche#explorer_en_graphe_A_Heuristique_Euclidienne(Probleme,Strategie)
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @throws Echec Si le problème ne peut pas se résoudre
     */
	public static void explorer_en_arbre(Probleme probleme, Strategie strategie) throws Echec
	{
		Noeud noeudTrouvee;
		String solutionTrouvee;
		String fichier;
		long debut;
		long fin;
		long temps;

		try
		{	
			debut = System.currentTimeMillis();
			noeudTrouvee = explorer_en_graphe_profondeur(probleme,strategie);
			fin = System.currentTimeMillis();
			temps = (fin - debut);
			
			solutionTrouvee = noeudTrouvee.toString() + "\nNombre de noeud crées " + Noeud.getNombreDeNoeudCree()
			+ " avec une série d'actions " + strategie.toString() + " a chaque noeud pour réaliser le chemin suivant : \n" + noeudTrouvee.getChemin()
			+ "\n entrainant un cout total de : "+noeudTrouvee.getCout() + " pour une durée de " + temps + "ms";
			
			fichier = "soluces/solution_en_profondeur_" + probleme.getEtatInitiale().getClass().getName() + probleme.getEtatInitiale().hashCode() + strategie.toString() + ".txt";
			Solution.ecrireSolution(solutionTrouvee, fichier, false);
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage());
		}
		
		try
		{
			debut = System.currentTimeMillis();
			noeudTrouvee = explorer_en_graphe_largeur(probleme,strategie);
			fin = System.currentTimeMillis();
			temps = (fin - debut);
			
			solutionTrouvee = noeudTrouvee.toString() + "\nNombre de noeud crées " + Noeud.getNombreDeNoeudCree()
			+ " avec une série d'actions " + strategie.toString() + " a chaque noeud pour réaliser le chemin suivant : \n" + noeudTrouvee.getChemin()
			+ "\n entrainant un cout total de : "+noeudTrouvee.getCout() + " pour une durée de " + temps + "ms";;
			
			fichier = "soluces/solution_en_largeur_" + probleme.getEtatInitiale().getClass().getName() + probleme.getEtatInitiale().hashCode() + strategie.toString() + ".txt";
			Solution.ecrireSolution(solutionTrouvee, fichier, false);
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage());
		}
		
		try
		{
			debut = System.currentTimeMillis();
			noeudTrouvee = explorer_en_graphe_A_Heuristique_Manhattan(probleme,strategie);
			fin = System.currentTimeMillis();
			temps = (fin - debut);
			
			solutionTrouvee = noeudTrouvee.toString() + "\nNombre de noeud crées " + Noeud.getNombreDeNoeudCree()
			+ " avec une série d'actions " + strategie.toString() + " a chaque noeud pour réaliser le chemin suivant : \n" + noeudTrouvee.getChemin()
			+ "\n entrainant un cout total de : "+noeudTrouvee.getCout() + " pour une durée de " + temps + "ms";
			
			fichier = "soluces/solution_en_A*_Manhattan_" + probleme.getEtatInitiale().getClass().getName() + probleme.getEtatInitiale().hashCode() +  strategie.toString() + ".txt";
			Solution.ecrireSolution(solutionTrouvee, fichier, false);
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage());
		}
		
		
		try
		{
			debut = System.currentTimeMillis();
			noeudTrouvee = explorer_en_graphe_A_Heuristique_Euclidienne(probleme,strategie);
			fin = System.currentTimeMillis();
			temps = (fin - debut);
			
			solutionTrouvee = noeudTrouvee.toString() + "\nNombre de noeud crées " + Noeud.getNombreDeNoeudCree()
			+ " avec une série d'actions " + strategie.toString() + " a chaque noeud pour réaliser le chemin suivant : \n" + noeudTrouvee.getChemin()
			+ "\n entrainant un cout total de : "+noeudTrouvee.getCout() + " pour une durée de " + temps + "ms";;
			
			fichier = "soluces/solution_en_A*_Euclidienne_" + probleme.getEtatInitiale().getClass().getName() + probleme.getEtatInitiale().hashCode() + strategie.toString() + ".txt";
			Solution.ecrireSolution(solutionTrouvee, fichier, false);
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage());
		}

	}
	
	/**
     * Méthode de classe qui permet d'explorer en profondeur le problème grâce à une stratégie 
     * mis en place pour trouver la solution avec une pile
     * @see Recherche#explorer_en_graphe(Probleme, Strategie, Frontiere)
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @return le noeud qui permet de résoudre le problème
     * @throws Echec Si le problème ne peut pas se résoudre avec une pile
     */
	public static Noeud explorer_en_graphe_profondeur(Probleme probleme, Strategie strategie) throws Echec
	{
		Noeud noeudTrouvee = null;
		try
		{
			//On instancie une pile
			Frontiere frontiere = new Frontiere(1);
			noeudTrouvee = explorer_en_graphe(probleme, strategie, frontiere);
		}
		catch(FrontiereException e)
		{
			throw new Echec(e.getMessage());
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage() + " dans l'exploration en profondeur selon la stratégie " + strategie.toString());
		}
		return noeudTrouvee;
	}
	
	/**
     * Méthode de classe qui permet d'explorer en largueur le problème grâce à une stratégie 
     * mis en place pour trouver la solution avec une file
     * @see Recherche#explorer_en_graphe(Probleme, Strategie, Frontiere)
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @return le noeud qui permet de résoudre le problème
     * @throws Echec Si le problème ne peut pas se résoudre avec une file
     */
	public static Noeud explorer_en_graphe_largeur(Probleme probleme, Strategie strategie) throws Echec
	{
		Noeud noeudTrouvee = null;
		try
		{
			//On instancie une file
			Frontiere frontiere = new Frontiere(2);
			noeudTrouvee = explorer_en_graphe(probleme, strategie, frontiere);
		}
		catch(FrontiereException e)
		{
			throw new Echec(e.getMessage());
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage() + " dans l'exploration en largeur selon la stratégie " + strategie.toString());
		}
		return noeudTrouvee;
	}
	
	/**
     * Méthode de classe qui permet d'explorer en A* avec une fonction heuristique de Mahanttan le problème grâce à une stratégie 
     * mis en place pour trouver la solution avec une file prioritaire
     * @see Recherche#explorer_en_graphe(Probleme, Strategie, Frontiere)
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @return le noeud qui permet de résoudre le problème
     * @throws Echec Si le problème ne peut pas se résoudre avec une file prioritaire
     */
	public static Noeud explorer_en_graphe_A_Heuristique_Manhattan(Probleme probleme, Strategie strategie) throws Echec
	{
		Noeud noeudTrouvee = null;
		try
		{
			//On instancie une file prioritaire
			Frontiere frontiere = new Frontiere(3);
			//Avec l'heuristique de Mahanttan
			Noeud.setChoixHeuristique(1);
			noeudTrouvee = explorer_en_graphe(probleme, strategie, frontiere);
		}
		catch(FrontiereException e)
		{
			throw new Echec(e.getMessage());
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage() + " dans l'exploration en A* avec l' heuristique Manhattan selon la stratégie " + strategie.toString());
		}
		return noeudTrouvee;
	}
	
	/**
     * Méthode de classe qui permet d'explorer en A* avec une fonction heuristique Euclidienne le problème grâce à une stratégie 
     * mis en place pour trouver la solution avec une file prioritaire
     * @see Recherche#explorer_en_graphe(Probleme, Strategie, Frontiere)
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @return le noeud qui permet de résoudre le problème
     * @throws Echec Si le problème ne peut pas se résoudre avec une file prioritaire
     */
	public static Noeud explorer_en_graphe_A_Heuristique_Euclidienne(Probleme probleme, Strategie strategie) throws Echec
	{
		Noeud noeudTrouvee = null;
		try
		{
			//On instancie une file prioritaire
			Frontiere frontiere = new Frontiere(3);
			//Avec l'heuristique de Euclidienne
			Noeud.setChoixHeuristique(2);
			noeudTrouvee = explorer_en_graphe(probleme, strategie, frontiere);
		}
		catch(FrontiereException e)
		{
			throw new Echec(e.getMessage());
		}
		catch(Echec e)
		{
			throw new Echec(e.getMessage() + " dans l'exploration en A* avec l'heuristique Euclidienne selon la stratégie " + strategie.toString());
		}
		return noeudTrouvee;
	}
	
	/**
     * Méthode de classe qui permet résoudre le problème grâce à une stratégie 
     * mis en place pour trouver la solution selon la frontiere opté
     * @param probleme que l'on doit résoudre
     * @param strategie qu'on a opter (d'abord déplacement bas ou haut, etc.)
     * @param frontiere pour stocker les noeuds lors du parcours
     * @return le noeud qui permet de résoudre le problème
     * @throws Echec Si le problème ne peut pas se résoudre
     */
	public static Noeud explorer_en_graphe(Probleme probleme, Strategie strategie, Frontiere frontiere) throws Echec
	{
		boolean developperFeuilles = true;
		boolean trouverSolution = false;
		int limite = 0;
		ArrayList<Noeud> listeFerme = new ArrayList<Noeud>();
		Noeud.setNombreDeNoeudCree(0);
		Noeud noeud = null;
		Noeud racine = new Noeud(probleme.getEtatInitiale(),null,"aucun",0,0);
		frontiere.ajouter(racine);

		while((!trouverSolution) && limite < 10000)
		{
			noeud = frontiere.supprimer();
			
			if(noeud == null)
			{
				throw new Echec("Le problème ne comporte aucune solution");
			}
			else
			{
				trouverSolution = verifierSolution(noeud.getEtat(),probleme);
						
				if(!trouverSolution)
				{
					developperFeuilles = verifierDeveloppementFeuilles(noeud,listeFerme);
							
					if(developperFeuilles)
					{
						listeFerme.add(noeud);
						developperFeuilles(noeud, strategie, frontiere);
					}
					
					developperFeuilles = true;
				}
			}
			limite++;
		}
		
		return noeud;
	}
	
	/**
     * Méthode de classe qui permet de vérifier si la situation courante est
     * dans les états finaux du problème
     * @param etat que l'on souhaite vérifier s'il répond à la solution ou non
     * @param probleme qui contient l'ensemble des états finaux
     * @return true ou false
     */
	public static boolean verifierSolution(Situation etat, Probleme probleme)
	{
		
		for(int i=0 ; i<probleme.getNombreEtatsFinaux() ; i++)
		{
			if(etat.equals(probleme.getEtatFinale(i)))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
     * Méthode de classe qui permet de vérifier le noeud courant est déjà
     * dans la liste fermée ou non, pour éviter de le redévelopper
     * @param listeFerme qui mémorise tous les noeuds développés
     * @param noeud qui permet de vérifier s'il est déjà présent ou pas
     * @return true ou false
     */
	public static boolean verifierDeveloppementFeuilles(Noeud noeud, ArrayList<Noeud> listeFerme)
	{
		
		for(Noeud noeudFermee : listeFerme)
		{
			if(noeud.equals(noeudFermee))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
     * Méthode de classe qui permet de développer les feuilles grâce au(x) opérateur(s) du noeud courant
     * selon la stratégie mis en place et de les ajouter dans la frontiere
     * @param noeud dont on veut développer ses feuilles
     * @param strategie pour connaitre l'ordre de création des noeuds feuilles
     * @param frontiere pour ajouter les feuilles crées
     */
	public static void developperFeuilles(Noeud noeud, Strategie strategie, Frontiere frontiere)
	{
		for(int i=0 ; i<strategie.getNombreActions() ; i++)
		{
			if(noeud.getEtat().verifierOperateurs(strategie.getAction(i)))
			{
				
				Noeud feuille = new Noeud(noeud.getEtat().cloner(),noeud," ",noeud.getCout()+1,noeud.getProfondeur()+1);
				String action = feuille.getEtat().appliquerOperateurs(strategie.getAction(i));
				feuille.setAction(action);
				frontiere.ajouter(feuille);
			}
		}
	}

}
