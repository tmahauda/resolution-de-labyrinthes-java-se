package solution;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe FrontiereException permet de lancer une exception relative à la classe Frontiere
* lorsqu'il n'a pas compris la structure de données à employer
* @see Frontiere
*/
@SuppressWarnings("serial")
public class FrontiereException extends Exception
{
	public FrontiereException(String message)
	{
		super(message);
	}
}
