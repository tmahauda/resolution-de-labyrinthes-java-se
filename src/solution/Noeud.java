package solution;
import situation.Situation;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Noeud implémentant l'interface Comparable pour la structure de données file prioritaire
* permet de créer les états de la situation
* @see Comparable
*/
public class Noeud implements Comparable<Noeud>
{
	/**
     * L'état de l'espace des états auquel le noeud correspond
     */
	private Situation etat;
	
	/**
     * Le noeud de l'arbre de recherche qui a généré ce noeud
     */
	private Noeud noeudParent;
	
	/**
     * L'action qui a été appliquée au parent pour générer le noeud
     */
	private String action;
	
	/**
     * Le coût du chemin de la racine de l'arbre jusqu'au noeud
     */
	private int cout;
	
	/**
     * Le nombre d'étapes que compte le chemin de la racine jusqu'au noeud
     */
	private int profondeur;
	
	/**
     * Le nombre de noeud crée qui permet de savoir combien de noeud a été crée durant l'ensemble du parcours
     */
	private static int nombreDeNoeudCree;
	
	/**
     * Une liste d'actions ordonnée pour appliquer les opérateurs
     */
	private static int choixHeuristique;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe:
     * @param etat l'état
     * @param noeudParent le noeud parent
     * @param action l'action
     * @param cout le cout
     * @param profondeur la profondeur
     * @see Situation la situation
     */
	public Noeud(Situation etat, Noeud noeudParent, String action, int cout, int profondeur)
	{
		this.etat = etat;
		this.noeudParent = noeudParent;
		this.action = action;
		this.cout = cout;
		this.profondeur = profondeur;
		nombreDeNoeudCree++;
	}
	
	public static int getNombreDeNoeudCree()
	{
		return nombreDeNoeudCree;
	}
	
	public static void setNombreDeNoeudCree(int nombre)
	{
		nombreDeNoeudCree = nombre;
	}
	
	public static int getChoixHeuristique()
	{
		return choixHeuristique;
	}
	
	public static void setChoixHeuristique(int choix)
	{
		choixHeuristique = choix;
	}
	
	public Situation getEtat()
	{
		return this.etat;
	}
	
	public void setEtat(Situation etat)
	{
		this.etat = etat;
	}
	
	public Noeud getNoeudParent()
	{
		return this.noeudParent;
	}
	
	public String getAction()
	{
		return this.action;
	}
	
	public void setAction(String action)
	{
		this.action = action;
	}
	
	public int getCout()
	{
		return this.cout;
	}
	
	public void setCout(int cout)
	{
		this.cout = cout;
	}
	
	public int getProfondeur()
	{
		return this.profondeur;
	}
	
	public void setProfondeur(int profondeur)
	{
		this.profondeur = profondeur;
	}
	
	@Override
	public String toString()
	{
		String noeudParent = "";
		if(this.noeudParent != null) noeudParent = this.noeudParent.toString();
		String etat = "\n" + this.etat.toString() + "\n";
		String profondeur = "Pronfondeur : " + this.profondeur + "\n";
		String action = "Opérateur : " + this.action + "\n";
		String cout = "Cout : " + String.valueOf(this.cout) + "\n";
		
		return noeudParent+etat+profondeur+action+cout;
	}
	
	public String getChemin()
	{
		String chemin = "";
		if(this.noeudParent != null) chemin = this.noeudParent.getChemin() + " ; " + this.action;
		
		return chemin;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!(obj instanceof Noeud))
			return false;
		Noeud other = (Noeud)obj;
		if (this.getEtat().equals(other.getEtat())) 
		{
			return true;
		}
		else return false;
	}
	
	/**
     * Méthode qui permet de trier une file prioritaire
     * - Si le noeud n a une valeur faible, alors il est prioritaire
     * - En cas d'égalité, si le noeud n est plus proche de la sortie, alors il est prioritaire
     * - Sinon il n'est pas prioritaire
     * @param n le noeud à comparer
     * @return action
     */
	@Override
	public int compareTo(Noeud n)
	{
		if(this.etat.calculerFHeuristique(choixHeuristique) > n.etat.calculerFHeuristique(choixHeuristique)) return 1;
		else if(this.etat.calculerFHeuristique(choixHeuristique) == n.etat.calculerFHeuristique(choixHeuristique)) 
		{
			if(this.etat.calculerHHeuristique(choixHeuristique) > n.etat.calculerHHeuristique(choixHeuristique)) return 1;
			else if(this.etat.calculerHHeuristique(choixHeuristique) == n.etat.calculerHHeuristique(choixHeuristique)) return 0;
			else return -1;
		}
		else return -1;
	}
}
