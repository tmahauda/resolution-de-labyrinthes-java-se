package solution;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe static Solution permet d'écrire la solution dans un fichier
*/
public class Solution 
{
	/**
     * Constructeur privée qui évite l'instanciation en objet. Cette classe comporte uniquement des méthodes de classes
     */
	private Solution(){}
	
	/**
	 * Méthode qui permet d'écrire une solution dans un fichier (en l'écrasant ou en le concaténant).
	 * @param solution à écrire dans le fichier
	 * @param fichier le nom du fichier à créer
	 * @param ajoute true si le texte doit être ajouté à la fin du fichier, sinon false.
	 */
	public static void ecrireSolution(String solution, String fichier, boolean ajoute) 
	{
		PrintWriter out = null;

		int finRepertoire = fichier.lastIndexOf(File.separator);
		String repertoire = "";
		if (finRepertoire != -1) 
		{
			repertoire = fichier.substring(0, finRepertoire);
			creerRepertoire(repertoire); 
		}
		
		try 
		{
			out = new PrintWriter(new FileOutputStream(new File(fichier), ajoute));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		
		out.print(solution);
		out.close();
	}

	
	/**
	 * Methode qui crée un répertoire
	 * @param chemin du répertoire à créer
	 */
	public static void creerRepertoire(String chemin) 
	{
		File repertoire = new File(chemin);

		//si le répertoire n'existe pas, alors on le crée
		if (!repertoire.exists()) 
		{
			System.err.println("Info: création d'un répertoire: " + chemin);
			boolean result = repertoire.mkdir();  

			if(!result) 
			{    
				System.err.println("Erreur: probleme lors de la création du répertoire: " + repertoire);  
			}
		}
	}
	
}
