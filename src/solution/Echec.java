package solution;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Echec permet de lancer une exception relative à la classe Recherche lorsqu'il n'y a pas de solution
*/
@SuppressWarnings("serial")
public class Echec extends Exception 
{
	public Echec(String message)
	{
		super(message);
	}
}
