package solution;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

/**
* @author MAHAUDA Théo
* @version 1.0
* 
* La classe Frontiere représente la collection des noeuds générés mais non encore explorés utilisée par l'algorithme de recherche
* @see Recherche#explorer_en_graphe(probleme.Probleme, probleme.Strategie, Frontiere)
*/
public class Frontiere 
{
	/**
     * Une pile représenté par une structure LinkedList pour le parcours en profondeur
     */
	private LinkedList<Noeud> pile;
	
	/**
     * Une file représenté par une structure LinkedList pour le parcours en largeur
     */
	private LinkedList<Noeud> file;
	
	/**
     * Une file prioritère par une structure PriorityQueue pour le parcours en A*
     */
	private PriorityQueue<Noeud> file_priorite;
	
	/**
     * Constructeur qui permet d'instancier les attributs de la classe selon une condition :
     * - Si le parametre choix vaut 1, alors la frontiere est de type pile
     * - Si le parametre choix vaut 2, alors la frontiere est de type file
     * - Si le parametre choix vaut 3, alors la frontiere est de type file_priorite
     * - Sinon la structure de données n'est pas reconnu
     * @param choix pour choisir le type de structure de données
     * @throws FrontiereException Si la structure de données n'est pas reconnu
     */
	public Frontiere(int choix) throws FrontiereException
	{
		switch(choix)
		{
		  case 1:
			  this.pile = new LinkedList<Noeud>();
			  this.pile.clear();
			  this.file = null;
			  this.file_priorite = null;
			  break;
		  case 2:
			  this.pile = null;
			  this.file = new LinkedList<Noeud>();
			  this.file.clear();
			  this.file_priorite = null;
			  break;
		  case 3:
			  this.pile = null;
			  this.file = null;
			  this.file_priorite = new PriorityQueue<Noeud>();
			  this.file_priorite.clear();
			  break;
		  default:
			  throw new FrontiereException("Structure de données non renconnu");
		}
	}
	
	/**
     * Méthode qui permet de supprimer et de récupérer le noeud en tête de la structure
     * @return noeud en tête
     */
	public Noeud supprimer()
	{
		Noeud noeud = null;
		try
		{
			if(this.pile != null) noeud = this.pile.removeFirst();
			else if(this.file != null) noeud = this.file.remove();
			else noeud = this.file_priorite.remove();
		}
		catch(NoSuchElementException e)
		{
			noeud = null;
		}
		return noeud;
	}
	
	/**
     * Méthode qui permet d'ajouter un noeud en tête de la structure
     * @param n le noeud à ajouter
     */
	public void ajouter(Noeud n)
	{
		if(this.pile != null) this.pile.addFirst(n);
		else if(this.file != null) this.file.add(n);
		else this.file_priorite.add(n);
	}
	
	/**
     * Méthode qui permet de récupérer la taille de la structure
     * @return la taille
     */
	public int getTaille()
	{
		if(this.pile != null) return this.pile.size();
		else if(this.file != null) return this.file.size();
		else return this.file_priorite.size();
	}
	
	/**
     * Méthode qui permet de récupérer le noeud en tête de structure sans pour autant la supprimer
     * @return le noeud en tête
     */
	public Noeud getNoeud()
	{
		if(this.pile != null) return this.pile.getFirst();
		else if(this.file != null) return this.file.peek();
		else return this.file_priorite.peek();
	}
}
